package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {

    int max_size = 20;
    
    ArrayList<FridgeItem> items = new ArrayList<>();
    
    @Override
    public int totalSize() {
        return max_size;
    }

    @Override
    public int nItemsInFridge() {

        return items.size();
    }

    

    @Override
    public boolean placeIn(FridgeItem item) {
        if (items.size() < max_size) {
            items.add(item);
            return true;
        }
        return false;
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (!items.contains(item)){
            throw new NoSuchElementException();
        }
        items.remove(item);
        

        
    }


    @Override
    public void emptyFridge() {
        items.clear();
        
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        ArrayList<FridgeItem> expiredFood = new ArrayList<>();
        for(int i=0; i < nItemsInFridge(); i++) {
            FridgeItem item = items.get(i);
            if(item.hasExpired()) {
                expiredFood.add(item);
            }
        }
        for(FridgeItem expiredItem : expiredFood){
            items.remove(expiredItem);
        }

        return expiredFood;
    }
    
    
}
